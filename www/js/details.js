/**
 * Created by sumit on 11-05-2017.
 */
"use strict";
var pid, access_token, pdata;

function updateProductSpecHeader() {
    // console.log(pdata.product[0]);
    $(".pimage").attr("src", pdata.image_url);
    $(".pname").html(pdata.product_name);
    $(".brand").html("");
    $(".description").html(pdata.description);
    $(".bestprice").html("RS." + pdata.price);
    $(".beststore").attr("src","../images/" + (pdata.source).toLowerCase() + ".png");
    $(".buy").parent().attr("href", pdata.source_url);
}

function updateDetails() {
    var params = {};
    params._method = "GET";
    params.Auth = access_token;
    API.getUrl(apiUrl + "/api/product?device_id=" + API.deviceID + "&product_id=" + pid, params, function(err, result){
        if(!err && result) {
            pdata = (JSON.parse(result));
            pdata = pdata.product[0];
            updateProductSpecHeader();
        }
    });
}

function updateSimilarProducts() {
    var params={};
    params._method = "GET";
    params.Auth = access_token;
    API.getUrl(apiUrl + "/api/product/similar?device_id=" + API.deviceID + "&product_id=" + pid + "&page_no=1&clipwiser_per_page=20", params, function(err, result){
        if (!err && result) {
            var data = {}, similarProduct;
            similarProduct = JSON.parse(result);
            similarProduct = similarProduct.similar_products;
            data.product = similarProduct;
            for(var k in data.product) {
                (data.product[k]).source = ((data.product[k]).source).toLowerCase();
            }
            $("#similarProducts").html(Mustache.render(similarProductTemplate, data));
        }
    });
}

$(document).ready(function(){
    pid = (document.URL).split("?")[1];
    pid = pid.split("=")[1];
    var accessToken = readCookie("clip_access");
    if(accessToken && accessToken !== "undefined" && accessToken !== "null" ) {
        accessToken = JSON.parse(accessToken);
        access_token = accessToken.access_token;
        updateDetails();
        updateSimilarProducts();
    } else {
        window.location.href= "/";
    }
});

var similarProductTemplate =
    '{{#product}}<a href="details.html?pid={{product_id}}"><div class="col-md-3 col-sm-4 col-xs-6 column product-div">\
        <div class="row">\
            <div class="col-xs-12 col-sm-12 col-md-12 column mtop15 mbottom10">\
                <img src="{{image_url}}" class="img-responsive product-img" alt="{{product_name}}">\
            </div>\
            <div class="col-md-6 col-sm-6 col-xs-6 column valign border-right">\
                <img src="../images/{{source}}.png" class="img-responsive store-img" alt="{{source}}">\
            </div>\
            <div class="col-md-6 col-sm-6 col-xs-6 column valign">\
                <p class="text-center mbottom width100">Rs. {{price}}</p>\
            </div>\
            <div class="col-xs-12 col-sm-12 col-md-12 column valign mtop10">\
                <p class="text-center mbottom pname similarname width100">{{product_name}}</p>\
                <p></p>\
            </div>\
        </div>\
    </div></a>{{/product}}';
/**
 * Created by sumit on 30-04-2017.
 */
"use strict"
var apiUrl, clientID, secretID, API = {};
apiUrl = "http://clipwiser.duoex.com";
clientID = "1234";
secretID = "abcd";
API.deviceID = "1";

API.getUrl = function (url, params, resultCallback) {
    // console.log(url, params, JSON.stringify(resultCallback));
    $.ajax({
        type: params._method || 'GET',
        url: url,
        crossDomain: true,
        contentType: 'application/json',
        beforeSend : function( xhr ) {
            if (params && params.Auth) {
                xhr.setRequestHeader( "Authorization", params.Auth );
            }
        },
        success: function (data) {
            resultCallback(null, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var err = jqXHR.responseText;
            resultCallback(err);
        }
    });
};

API.queryUrl = function (url, body, dataObj, resultCallback) {
    body = body || {};
    body._method = dataObj._method || 'POST';

    $.ajax({
        type:'POST',
        async: true,
        crossDomain: true,
        url: url,
        method: 'POST',
        // processData: false,
        contentType: 'application/x-www-form-urlencoded',
        "data": body,
        beforeSend : function( xhr ) {
            if (dataObj && dataObj.Auth) {
                xhr.setRequestHeader( "Authorization", dataObj.Auth);
            }
            if (body && body.CORS) {
                xhr.setRequestHeader("" + body.CORS);
            }
        },

        success: function (data) {
            resultCallback(null, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var err = jqXHR.responseText;
            resultCallback(err);
        }
    });
};


/*function getAccessToken() {
    var params = {};
    params._method = "GET";
    API.getUrl(apiUrl + "/api/auth/get-token?client_id=" + clientID + "&client_secret=" + secretID + "&device_id=1", params, function(err, result){
        if (!err) {
            result = JSON.parse(result);
            API.accessToken = result.access_token;
            console.log(result.access_token)
        }
    });
}*/

$(document).ready(function(){
    $("#navbar").html(navbar);
    $(".drop-down").hover(function() {
        var id = $(this).attr("id");
        $("#" + id + "1" + ' div.mega-menu').addClass('display-on');
    });
    $(".nav-list li:nth-child(2),.nav-list li:nth-child(3),.nav-list li:nth-child(4), .form-group,.nav1").hover(function() {
        $("#mobiles-nav1" + ' div.mega-menu').removeClass('display-on');
    });
    $("#mobiles-nav1").mouseleave(function() {
        $("#mobiles-nav1" + ' div.mega-menu').removeClass('display-on');
    });

    $(".icon-search").click(function(){
        var text = $(".input-search").val();
        if (text && text.length > 3) {
            window.location.href = "search.html?q=" + text;
        } else {
            $.growl.error({ message: "Search text must be greater than three letters." });
        }
    });
    if ($((document.URL).indexOf("=") !== -1)) {
        console.log("here")
        $(".nav-list-active").removeClass("nav-list-active");
    }

    $(".coming").click(function(){
        window.location.href = "comingsoon.html"
    });
});

var navbar = '<nav class="navbar navbar-home">\
        <div class="container-fluid padding0 nav1 valign hidden-sm hidden-xs">\
            <span class="width100">\
                <span class="pull-right not-profile">\
                    <span class="mleft15 coming"><img src="../images/notification.png"> Notification</span>\
                    <span class="mleft15 coming"><img src="../images/wishlist.png"> Wishlist</span>\
                    <span class="mleft15 coming"><img src="../images/profile.png"> Profile</span>\
                </span>\
            </span>\
        </div>\
        <div class="container-fluid padding0 nav2 vertical-align">\
            <div class="clearfix mtop13 margin-xs">\
                <div class="col-md-2 col-sm-2 col-xs-2 column">\
                    <img src="//lh3.googleusercontent.com/HNn5y8Wd85--7LpaVomGYeyIJggityKg1ya9MiX9EEdGkS44Qtz_QEnDXIgY-SrKBbMr=w300-rw" class="img-responsive clip-logo">\
                </div>\
                <div class="col-md-5 col-sm-8 col-xs-8 column">\
                    <div class="form-group">\
                        <div class="input-group" id="navbar-search">\
                            <div class="input-group-btn searchclass-select-button">\
                                <input type="text" class="form-control search-nav input-search" placeholder="Enter keyword to search" id="autocomplete" autocomplete="off">\
                                <span class="glyphicon glyphicon-search navsearch icon-search"></span> </div>\
                        </div>\
                    </div>\
                </div>\
                <div class="col-md-5 col-sm-2 col-xs-2 column">\
                    <div class="dropdown hidden-md hidden-lg">\
                        <button class="btn btn-primary dropdown-toggle menu-btn" style="background:transparent;border:none;outline:none;" type="button" data-toggle="dropdown">\
                            <span class="fa fa-bars menu-bar"></span></button>\
                        <ul class="dropdown-menu menu-list">\
                            <li><a href="#">Categories</a></li>\
                            <li><a href="/">Clips</a></li>\
                            <li><a href="coming.html">Social</a></li>\
                            <li><a href="coming.html">Pages</a></li>\
                        </ul>\
                    </div>\
                    <ul class="nav-list hidden-xs hidden-sm">\
                        <li class="drop-down" id="mobiles-nav">\
                            <a href="javascript:void(0);"><span class="mright5"><i class="ionicons ion-bag"></i></span>Categories\
                            </a>\
                        </li>\
                        <li class="nav-list-active clips"><a href="/"><span class="mright5"><i class="material-icons">attachment</i></span>Clips</a></li>\
                        <li class="coming"><span class="fa fa-users mright5"></span>Social</li>\
                        <li class="coming"><span class="mright5"><i class="material-icons">pages</i></span>Pages</li>\
                    </ul>\
                </div>\
            </div>\
        </div>\
        <div class="container-fluid hidden-sm hidden-xs" style="background:#f2f2f2;">\
            <div class="row clearfix">\
                <div class="col-md-12 col-sm-12 column">\
                    <li id="mobiles-nav1" style="background:#f2f2f2;color:#f2f2f2;">\
                        <div class="mega-menu fadeIn animated hidden-sm hidden-xs">\
                            <ul class="categories"></ul>\
                        </div>\
                    </li>\
                </div>\
            </div>\
        </div>\
    </nav>';
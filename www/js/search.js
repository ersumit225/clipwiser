/**
 * Created by sumit on 13-05-2017.
 */
"use strict";
var category, access_token, pagecount = 1, category_id = 1;

function getAndUpdateProduct() {
    var params={};
    params._method = "GET";
    params.Auth = access_token;
    //calling search API, category_id or retailer_id is optional
    API.getUrl(apiUrl + "/api/search?device_id=" + API.deviceID + "&keyword=" + category + "&category_id=" + category_id + "&page_no=" + pagecount + "&clipwiser_per_page=40", params, function(err, result){
        if (!err && result) {
            var data = {}, similarProduct;
            similarProduct = JSON.parse(result);
            similarProduct = similarProduct.products;
            data.product = similarProduct;
            $("#count").html((data.product).length);
            for(var k in data.product) {
                (data.product[k]).source = ((data.product[k]).source).toLowerCase();
            }
            $("#searchResult").html(Mustache.render(similarProductTemplate, data));
        }
    });
}

$(document).ready(function(){
    if ((document.URL).indexOf("q=") !== -1) {
        category = (document.URL).split("q=")[1];
        // category = category.split("|");
        // category_id = category[1];
        // category = category[0];
    } else {
        category = (document.URL).split("category=")[1];
        category = category.split("|");
        category_id = category[1];
        category = category[0];
    }
    var accessToken = readCookie("clip_access");
    if(accessToken && accessToken !== "undefined" && accessToken !== "null" ) {
        accessToken = JSON.parse(accessToken);
        access_token = accessToken.access_token;
        getAndUpdateProduct();
    } else {
        window.location.href= "/";
    }

});

var similarProductTemplate =
    '{{#product}}<a href="details.html?pid={{product_id}}"><div class="col-md-3 col-sm-4 col-xs-6 column product-div">\
        <div class="row">\
            <div class="col-xs-12 col-sm-12 col-md-12 column mtop15 mbottom10 height150 vertical-align">\
                <img src="{{image_url}}" class="img-responsive product-img" alt="{{product_name}}">\
            </div>\
            <div class="col-md-6 col-sm-6 col-xs-6 column valign border-right">\
                <img src="../images/{{source}}.png" class="img-responsive store-img" alt="{{source}}">\
            </div>\
            <div class="col-md-6 col-sm-6 col-xs-6 column valign">\
                <p class="text-center mbottom width100">Rs. {{price}}</p>\
            </div>\
            <div class="col-xs-12 col-sm-12 col-md-12 column valign mtop10">\
                <p class="text-center mbottom pname similarname width100">{{product_name}}</p>\
                <p></p>\
            </div>\
        </div>\
    </div></a>{{/product}}';
/**
 * Created by sumit on 22-04-2017.
 */
"use strict";
var accessToken, udata;

function getAccessTokenAndUpdateCategory() {
    accessToken = readCookie("clip_access");
    console.log(accessToken);
    if(accessToken && accessToken !== "undefined" && accessToken !== "null" ) {
        console.log("parsing: " + accessToken);
        accessToken = JSON.parse(accessToken);
        accessToken = accessToken.access_token;
        udata = readCookie("clip_user");
        if (udata) {
            getAllCategory();
        } else {
            $("#login-modal").click();
            getAccessToken();
        }
    } else {
        if (accessToken && accessToken != "undefined") {
            console.log("accessToken is : " + accessToken);
            accessToken = accessToken.access_token;
            udata = readCookie("clip_user");
            if (udata) {
                getAllCategory();
            } else {
                $("#login-modal").click();
                getAccessToken();
            }
        }else {
            console.log("login modal clicked");
            $("#login-modal").click();
            getAccessToken();
        }
    }
}

function getAccessToken() {
    var params = {};
    params._method = "GET";
    API.getUrl(apiUrl + "/api/auth/get-token?client_id=" + clientID + "&client_secret=" + secretID + "&device_id=1", params, function(err, result){
        if (!err && result) {
            createCookie("clip_access", result, 365);
            result = JSON.parse(result);
            accessToken = result.access_token;
        }
    });
}

function getAllCategory() {
    var params = {};
    params._method = "GET";
    params.Auth = accessToken;

    API.getUrl(apiUrl + "/api/category/all?device_id=" + API.deviceID, params, function(err, result){
        if(!err) {
            var data = {};
            data.product = JSON.parse(result);
            data.product = data.product.all_categories;
            // console.log(data.product);
            // for (var k in data.product) {
            //     console.log((data.product)[k].name + ":" + (data.product)[k].display_name);
            // }
            $(".categories").html(Mustache.render(categories_Template, data));

        }
    });
    //API for get products by category
    API.getUrl(apiUrl + "/api/user?device_id=" + API.deviceID + "&ids=1&page_no=1&clipwiser_per_page=40", params, function(err, result){
        if(!err) {
            var data = {};
            data.product = JSON.parse(result);
            //console.log(data.product);

            data.product = data.product.products;
            $(".clipbadge").html((data.product).length);
            for(var k in data.product) {
                (data.product[k]).source = ((data.product[k]).source).toLowerCase();
            }
            $("#popularproduct").html(Mustache.render(productTemplate, data));

        }
    });
}

function registerUser() {
    var dataObj = {};

    var params = {
        device_id:"1",
        email:$("#umail").val(),
        password:$("#pwd").val(),
        fname:$("#fname").val(),
        lname:$("#lname").val(),
        mobile:$("#mobileNo").val(),
        dob:$("#dob").val(),
        gender:"M"
    };

    dataObj.Auth = accessToken;
    dataObj._method = "POST";
    // console.log(params);
    API.queryUrl(apiUrl + "/api/user/register", params, dataObj, function(err, result) {
        if (!err) {
            console.log(result);
            createCookie("clip_user", result, 365);
            $(".modal").modal("hide");
            getAllCategory()
        }
    });
}

function loginUser() {
    var params = {
        device_id : "1",
        email : $("#log-email").val(),
        password : $("#log-pwd").val()
    };

    var dataObj = {};
    dataObj._method = "POST";
    dataObj.Auth = accessToken;

    API.queryUrl(apiUrl + "/api/user/login", params, dataObj, function(err, result){
        if(!err) {
            console.log(result);
            createCookie("clip_user", result, 365);
            $(".modal").modal("hide");
            getAllCategory();
        }
    });
}

$("#register").click(function(){
    registerUser();
});
$("#login").click(function(){
    loginUser();
});

$(document).ready(function(){
    getAccessTokenAndUpdateCategory();
});

var productTemplate =
    '{{#product}}<a href="details.html?pid={{product_id}}"><div class="col-md-3 col-sm-4 col-xs-6 column product-div">\
        <div class="row">\
            <div class="col-xs-12 col-sm-12 col-md-12 column mtop15 mbottom10 height150 vertical-align">\
                <img src="{{image_url}}" class="img-responsive product-img" alt="{{product_name}}">\
            </div>\
            <div class="col-md-6 col-sm-6 col-xs-6 column valign border-right">\
                <img src="../images/{{source}}.png" class="img-responsive store-img" alt="{{source}}">\
            </div>\
            <div class="col-md-6 col-sm-6 col-xs-6 column valign padding0">\
                <p class="text-center mbottom width100">Rs. {{price}}</p>\
            </div>\
            <div class="col-xs-12 col-sm-12 col-md-12 column valign mtop10 pname-div">\
                <p class="text-center mbottom pname width100">{{product_name}}</p>\
                <p></p>\
            </div>\
        </div>\
    </div></a>{{/product}}';

var categories_Template = '<ul>{{#product}}<li><a href="search.html?category={{name}}|{{category_id}}">{{display_name}}</a></li>{{/product}}</ul>';
